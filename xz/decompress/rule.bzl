visibility("//xz/...")

DOC = """A Lempel–Ziv–Markov compressed file to be decompressed."

```py
xz_decompress(
    name = "decompress",
    src = ":archive.xz",
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "Decompresses a Lempel–Ziv–Markov compressed file.",
        mandatory = True,
        allow_single_file = [".xz"],
    ),
    "_template": attr.label(
        default = "@rules_coreutils//coreutils/redirect:stdout",
        allow_single_file = True,
    ),
}

def implementation(ctx):
    xz = ctx.toolchains["//xz/toolchain/xz:type"]

    output = ctx.actions.declare_file(ctx.file.src.basename.removesuffix(".xz"))
    rendered = ctx.actions.declare_file("{}.{}".format(ctx.label.name, ctx.file._template.extension))

    ctx.actions.expand_template(
        template = ctx.file._template,
        output = rendered,
        is_executable = True,
        substitutions = {
            "{{stdout}}": output.path,
        },
    )

    args = ctx.actions.args()
    args.add(xz.executable.path)
    args.add("-cdfk").add(ctx.file.src)

    ctx.actions.run(
        outputs = [output],
        inputs = [ctx.file.src],
        arguments = [args],
        tools = [xz.run],
        executable = rendered,
        toolchain = "//xz/toolchain/xz:type",
        mnemonic = "xzDecompress",
        progress_message = "Decompressing %{input} to %{output}",
    )

    return DefaultInfo(files = depset([output]))

xz_decompress = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//xz/toolchain/xz:type"],
)

decompress = xz_decompress
