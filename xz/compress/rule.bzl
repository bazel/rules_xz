visibility("//xz/...")

DOC = """Compresses a file with the Lempel–Ziv–Markov (`xz`) algorithm.

```py
xz_compress(
    name = "compress",
    src = "some/file.txt",
    level = 0-9,
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "A file to compress with the Lempel–Ziv–Markov algorithm.",
        mandatory = True,
        allow_single_file = True,
    ),
    "level": attr.int(
        doc = "The compression level.",
        mandatory = False,
        default = 6,
        values = [l for l in range(0, 10)],
    ),
    "_template": attr.label(
        default = "@rules_coreutils//coreutils/redirect:stdout",
        allow_single_file = True,
    ),
}

def implementation(ctx):
    xz = ctx.toolchains["//xz/toolchain/xz:type"]

    output = ctx.actions.declare_file("{}.xz".format(ctx.file.src.basename))
    rendered = ctx.actions.declare_file("{}.{}".format(ctx.label.name, ctx.file._template.extension))

    ctx.actions.expand_template(
        template = ctx.file._template,
        output = rendered,
        is_executable = True,
        substitutions = {
            "{{stdout}}": output.path,
        },
    )

    args = ctx.actions.args()
    args.add(xz.executable.path)
    args.add("-czfk").add(ctx.file.src)
    args.add("-{}".format(ctx.attr.level))

    ctx.actions.run(
        outputs = [output],
        inputs = [ctx.file.src],
        arguments = [args],
        tools = [xz.run],
        executable = rendered,
        toolchain = "//xz/toolchain/xz:type",
        mnemonic = "xzCompress",
        progress_message = "Compressing %{input} to %{output}",
    )

    return DefaultInfo(files = depset([output]))

xz_compress = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//xz/toolchain/xz:type"],
)

compress = xz_compress
